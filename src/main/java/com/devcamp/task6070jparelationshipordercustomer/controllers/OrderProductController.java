package com.devcamp.task6070jparelationshipordercustomer.controllers;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task6070jparelationshipordercustomer.model.Product;
import com.devcamp.task6070jparelationshipordercustomer.service.OrderService;
import com.devcamp.task6070jparelationshipordercustomer.service.ProductService;

@RestController
@CrossOrigin
public class OrderProductController {
    @Autowired
    OrderService orderService;
    @Autowired
    ProductService productService;
    //get order by customer id
    @GetMapping("/devcamp-products")
    public ResponseEntity<Set<Product>> getProductsByOrderIdApi(@RequestParam(value = "orderId") long orderId){
        try {
            Set<Product> orderProducts = orderService.getProductsByOrderId(orderId);
            if (orderProducts != null ){
                return new ResponseEntity<>(orderProducts, HttpStatus.OK);
            }
            else return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    //get all product list
    @GetMapping("/all-products")
    public ResponseEntity<List<Product>> getAllProductsApi(){
        try {
            return new ResponseEntity<>(productService.getAllProducts(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }
}
